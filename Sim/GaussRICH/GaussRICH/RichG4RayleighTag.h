#ifndef       GaussRICH_RichG4RaylieghTag_h
#define       GaussRICH_RichG4RayleighTag_h 1 
#include "Geant4/G4Track.hh"
extern G4int RichG4RayleighTag(const G4Track& aPhotTrack);
#endif // GaussRICH_RichG4RaylieghTag_h


